import { Component, OnInit } from '@angular/core';
import { FormGroup ,FormBuilder,Validators,NgForm} from '@angular/forms';
import {NgbPaginationModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import { Flight } from '../flight';
import { ServiceShareService } from '../service-share.service';
import {  } from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { TimeAgoPipe } from 'time-ago-pipe';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css'],

})
export class BookingComponent implements OnInit {
  flight : Flight;
  flights : Array<Flight>=[];
  flightform ! : FormGroup;
  todayDate : Date = new Date();


  constructor(private fb: FormBuilder, private PageService: ServiceShareService) {
    this.flight =new Flight('','','','',0,this.todayDate,0,0,this.todayDate);
    this.flights = PageService.getData();
   }

  ngOnInit(): void {

    this.flightform = this.fb.group({
      Name: ['', [Validators.required, Validators.minLength(1)]],
      From: ['', [Validators.required]],
      To: ['', [Validators.required]],
      Type: ['', [Validators.required]],
      Adults: ['', [ Validators.maxLength(10)]],
      Departure:['',[Validators.required]],
      Children:['',[ Validators.maxLength(10)]],
      Infants: ['',[ Validators.maxLength(10)]],
      Arrival: ['',[Validators.required]]
    })
    this.flights = this.PageService.getData()


  }



  onSubmit(f:FormGroup): void{


      let form_record = new Flight(f.get('Name')?.value,
                                    f.get('From')?.value,
                                    f.get('To')?.value,
                                    f.get('Type')?.value,
                                    f.get('Adults')?.value,
                                    f.get('Departure')?.value,
                                    f.get('Children')?.value,
                                    f.get('Infants')?.value,
                                    f.get('Arrival')?.value);
      this.PageService.adddata(form_record);

    }

}










