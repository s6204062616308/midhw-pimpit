import { Injectable } from '@angular/core';
import { Flight } from './flight';
import { Mockdata } from './mockdata';
@Injectable({
  providedIn: 'root'
})
export class ServiceShareService {
  Data: Flight[] = [];
  constructor() {
    this.Data = Mockdata.moc;
  }

  getData(): Flight[] {
    return this.Data;
  }

  adddata(flights:Flight){
    this.Data.push(flights)
  }


}
