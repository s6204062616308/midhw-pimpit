export class Flight {
    public fullName: string;
    public from: string;
    public to: string;
    public type: string;
    public adults: number;
    public departure: Date;
    public children: number;
    public infants: number;
    public arrival: Date;

  constructor(fullName:string,from:string,to:string,type:string,
    adults:number,departure:Date, children:number,infants:number,arrival:Date,) {
    this.fullName = fullName;
    this.from = from;
    this.to = to;
    this.type= type;
    this.departure =departure;
    this.arrival = arrival;
    this.adults = adults;
    this.children = children;
    this.infants =infants

  }
}

