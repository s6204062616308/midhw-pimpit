import { Flight } from "./flight";


export class Mockdata {
  public static moc:Flight[]=[
    {
      fullName: "dokmai",
      from: "A",
      to: "B",
      type: "One way",
      adults: 1,
      departure: new Date,
      children: 2,
      infants: 0,
      arrival: new Date
    },
    {
      fullName: "phatwo",
      from: "c",
      to: "z",
      type: "One way",
      adults: 3,
      departure: new Date,
      children: 0,
      infants: 0,
      arrival: new Date

    },
  ]

}
